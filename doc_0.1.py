
# coding: utf-8

# In[2]:


from __future__ import division  # utile per la divisione in decimali
import warnings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score, precision_score, accuracy_score, roc_curve, auc

# ignore warnigs due to deprecated method when using v. 3.* intead of 2.*
warnings.simplefilter('ignore')


# ### TOOLS

# In[3]:


def compute_metrics(actual, predictions, predictions_scores):
    cm1 = confusion_matrix(y_true=actual, y_pred=predictions)
    tn, fp, fn, tp = cm1.ravel()
    
    false_positive_rate, true_positive_rate, thresholds = roc_curve(actual, predictions_scores)
    print("AUC: %0.2f" % auc(false_positive_rate, true_positive_rate) )
    print("SPECIFICITY: %0.2f" % (tn/(tn+fp)))
    print("SENSITIVITY: %0.2f" % (tp/(tp+fn)) )
    print("PRECISION: %0.2f" % (precision_score(actual, predictions)) )
    print("ACCURACY: %0.2f" % (accuracy_score(actual, predictions)) )


# In[4]:


def compute_prediction_classes_count(predictions):
    x = np.bincount(predictions)
    ii = np.nonzero(x)[0]
    print("\nPREDICTIONS FOR EVERY CLASS:")
    print(np.vstack((ii,x[ii])).T)


# In[5]:


from sklearn.metrics import make_scorer, matthews_corrcoef

def specificity(y_true, y_pred):
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
    tn, fp, fn, tp = cm.ravel()
    return tn/(tn+fp)

def sensitivity(y_true, y_pred):
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
    tn, fp, fn, tp = cm.ravel()
    return tp/(tp+fn)

scoring = {'Accuracy': 'accuracy',
           'Precision': 'average_precision',
           'ROC-AUC' : make_scorer(roc_auc_score), 
           'Matthews Correlation Coefficient': make_scorer(matthews_corrcoef),
           'Sensitivity': make_scorer(sensitivity),
           'Specificity' : make_scorer(specificity)
          }


# ## Dataset

# In[6]:


dataset1 = pd.read_excel("/Users/antonio/GitHub/ia/data.xlsx")
dataset1.head()


# Eliminazione dal dataset delle colonne non utili

# In[7]:


dataset = dataset1.drop(columns = ["ID_studente", "Voto_scuola_sup", "Situazione_OFA"] )
X = dataset.drop('Abbandoni', axis=1)
y = dataset['Abbandoni']


# Numero di valori per ogni classe

# In[8]:


target_count = y.value_counts()
print('Class 0:', target_count[0])
print('Class 1:', target_count[1])
print('Proportion:', round(target_count[0] / target_count[1], 2), ': 1')


# Split test-training set

# In[9]:


from sklearn.model_selection import train_test_split

#Being imbalanced classification, we cannot ensure the predictions will be correct for the minority class
#So we will perform a SMOTE to balance out the two classes. Lets split the dataset into train and test before that
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)


# Put here the algorithm for balancing dataset

# In[10]:


from imblearn.over_sampling import SMOTE
sm = SMOTE(random_state=1, k_neighbors=5, kind="borderline2")


# # Classifiers

# In the first part, for each algorithm, we tune the hyperparameters to select best classifier. 
# Then, this classifier is evalueted:
#  * with the entire (balanced) dataset (splitted in train/test set)
#  * using cross validation (performance values are a mean of each trained classifier)

# In[11]:


from imblearn.pipeline import Pipeline as imbPipeline
from sklearn.model_selection import GridSearchCV, cross_validate


# #### Random forest

# In[11]:


from sklearn.ensemble import RandomForestClassifier

pipe = imbPipeline([
    ('oversample', sm),
    ('clf', RandomForestClassifier(n_jobs=-1))
    ])

param_grid = {'clf__max_depth': list(range(5,10)),
              'clf__max_features': ['sqrt', 'log2'],
              'clf__bootstrap' : [True, False]}

gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
gs.fit(X_train, y_train)
print("Best params: %s\n" % gs.best_params_)

clf = gs.best_estimator_

clf.fit(X_train, y_train)
predictions = clf.predict(X_test)
predictions_scores = clf.predict_proba(X_test)[:,1]

compute_metrics(y_test, predictions, predictions_scores)
#compute_prediction_classes_count(predictions)


# In[20]:


pipe = imbPipeline([
    ('oversample', sm),
    ('clf', RandomForestClassifier(bootstrap = True,
                                   max_features = 'log2',
                                   max_depth = 9,
                                   n_jobs=-1))
    ])

scores = cross_validate(pipe, X, y, scoring=scoring, cv=10, return_train_score=False)

for i in sorted(scores.keys()):
    print("%s %s: %0.2f (+/- %0.2f)" % (i.split('_')[0].capitalize(), 
                                        i.split('_')[1].capitalize(), 
                                        scores[i].mean(), 
                                        scores[i].std() * 2))


# #### Logistic regression

# In[21]:


from sklearn.linear_model import LogisticRegression

pipe = imbPipeline([
    ('oversample', sm),
    ('clf', LogisticRegression(solver="liblinear", n_jobs=-1))
    ])

param_grid = {'clf__C': [0.01,0.1,1,10,100,1000,10000]}

gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
gs.fit(X_train, y_train)
print("Best params: %s\n" % gs.best_params_)

clf = gs.best_estimator_

clf.fit(X_train, y_train)
predictions = clf.predict(X_test)
predictions_scores = clf.predict_proba(X_test)[:,1]

compute_metrics(y_test, predictions, predictions_scores)
#compute_prediction_classes_count(predictions)


# In[22]:


pipe = imbPipeline([
    ('oversample', sm),
    ('clf', LogisticRegression(C=0.01, solver="liblinear", n_jobs=-1))
    ])

scores = cross_validate(pipe, X, y, scoring=scoring, cv=10, return_train_score=False)

for i in sorted(scores.keys()):
    print("%s %s: %0.2f (+/- %0.2f)" % (i.split('_')[0].capitalize(), 
                                        i.split('_')[1].capitalize(), 
                                        scores[i].mean(), 
                                        scores[i].std() * 2))


# #### Support Vector Machine

# In[23]:


from sklearn.svm import SVC

pipe = imbPipeline([
    ('oversample', sm),
    ('clf', SVC(probability=True, random_state=1))
    ])

param_grid = {'clf__C': [0.01,0.1,1,10,100,1000,10000]}

gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
gs.fit(X_train, y_train)
print("Best params: %s\n" % gs.best_params_)

clf = gs.best_estimator_

clf.fit(X_train, y_train)
predictions = clf.predict(X_test)
predictions_scores = clf.predict_proba(X_test)[:,1]

compute_metrics(y_test, predictions, predictions_scores)
#compute_prediction_classes_count(predictions)


# In[24]:


pipe = imbPipeline([
    ('oversample', sm),
    ('clf', SVC(C = 0.01, probability=True, random_state=1))
    ])

scores = cross_validate(clf, X, y, scoring=scoring, cv=10, return_train_score=False)

for i in sorted(scores.keys()):
    print("%s %s: %0.2f (+/- %0.2f)" % (i.split('_')[0].capitalize(), 
                                        i.split('_')[1].capitalize(), 
                                        scores[i].mean(), 
                                        scores[i].std() * 2))


# #### XGBoost

# Without tune hyperparameters (anbelivabol better)

# In[27]:


from sklearn.model_selection import RandomizedSearchCV
from xgboost import XGBClassifier

clf = imbPipeline([
    ('oversample', sm),
    ('clf', XGBClassifier(n_jobs=-1))
    ])

clf.fit(X_train.values, y_train)
predictions = clf.predict(X_test.values)
predictions_scores = clf.predict_proba(X_test.values)[:,1]

compute_metrics(y_test, predictions, predictions_scores)
#compute_prediction_classes_count(predictions)

scores = cross_validate(clf, X.values, y, scoring=scoring, cv=10, return_train_score=False)

print("\n")
for i in sorted(scores.keys()):
    print("%s %s: %0.2f (+/- %0.2f)" % (i.split('_')[0].capitalize(), 
                                        i.split('_')[1].capitalize(), 
                                        scores[i].mean(), 
                                        scores[i].std() * 2))


# First tuning for 4 hyperparameters

# In[18]:


from sklearn.model_selection import RandomizedSearchCV
from xgboost import XGBClassifier

pipe = imbPipeline([
    ('oversample', sm),
    ('clf', XGBClassifier(n_jobs=-1))
    ])

param_grid = {'clf__max_depth':range(3,10,2),
              'clf__min_child_weight':range(1,6,2),
              'clf__gamma':[i/10.0 for i in range(0,5)],
              'clf__learning_rate':[i/1000.0 for i in range(5,20,2)]
             }

gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
gs.fit(X_train.values, y_train)
print("Best params: %s\n" % gs.best_params_)

clf = gs.best_estimator_

clf.fit(X_train.values, y_train)
predictions = clf.predict(X_test.values)
predictions_scores = clf.predict_proba(X_test.values)[:,1]

compute_metrics(y_test, predictions, predictions_scores)
#compute_prediction_classes_count(predictions)


# Second tuning for other 2 hyperparameters

# In[22]:


pipe = imbPipeline([
    ('oversample', sm),
    ('clf', XGBClassifier(n_jobs=-1,
                         gamma= 0.0,
                         learning_rate= 0.019,
                         max_depth= 7,
                         clf__min_child_weight= 3))
    ])

param_grid = {'clf__subsample':[i/100.0 for i in range(55,70,5)],
              'clf__colsample_bytree':[i/100.0 for i in range(85,100,5)]
             }

gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
gs.fit(X_train.values, y_train)
print("Best params: %s\n" % gs.best_params_)

clf = gs.best_estimator_

clf.fit(X_train.values, y_train)
predictions = clf.predict(X_test.values)
predictions_scores = clf.predict_proba(X_test.values)[:,1]

compute_metrics(y_test, predictions, predictions_scores)


# In[23]:


scores = cross_validate(clf, X.values, y, scoring=scoring, cv=10, return_train_score=False)

for i in sorted(scores.keys()):
    print("%s %s: %0.2f (+/- %0.2f)" % (i.split('_')[0].capitalize(), 
                                        i.split('_')[1].capitalize(), 
                                        scores[i].mean(), 
                                        scores[i].std() * 2))

