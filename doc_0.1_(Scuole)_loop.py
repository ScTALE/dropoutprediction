
# coding: utf-8

# In[12]:


from __future__ import division  # utile per la divisione in decimali
import warnings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# ignore warnigs due to deprecated method when using v. 3.* intead of 2.*
#warnings.simplefilter('ignore')
warnings.filterwarnings('ignore')


# ## Dataset

# In[13]:


dataset1 = pd.read_excel("/Users/antonio/GitHub/ia/data.xlsx")
dataset1.head()


# Deleted unnecessary features

# In[15]:


dataset = dataset1.drop(columns = ["ID_studente", "Voto_scuola_sup", "Situazione_OFA"] )


# Group by "Scuole"

# In[16]:


scuole = dict(tuple(dataset.groupby('Scuola')))


# ### TOOLS

# This method is necessary to compute metrics when training on entire dataset

# In[17]:


from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score, precision_score, accuracy_score, roc_curve, auc

# calculate metrics for the training on entire dataset
def compute_metrics_es(actual, predictions, predictions_scores):
    cm1 = confusion_matrix(y_true=actual, y_pred=predictions)
    tn, fp, fn, tp = cm1.ravel()
    
    false_positive_rate, true_positive_rate, thresholds = roc_curve(actual, predictions_scores)
    
    acc = round(accuracy_score(actual, predictions), 2)
    prec = round(precision_score(actual, predictions), 2)
    roc_auc = round(auc(false_positive_rate, true_positive_rate), 2)
    sens = round(tp/(tp+fn), 2)
    spec = round(tn/(tn+fp), 2)
    
    return [acc, prec, roc_auc, sens, spec]


# Defined metrics when using CV

# In[18]:


from sklearn.metrics import make_scorer, matthews_corrcoef

def specificity(y_true, y_pred):
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
    tn, fp, fn, tp = cm.ravel()
    return tn/(tn+fp)

def sensitivity(y_true, y_pred):
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred)
    tn, fp, fn, tp = cm.ravel()
    return tp/(tp+fn)

scoring = {'Accuracy': 'accuracy',
           'Precision': 'average_precision',
           'ROC-AUC' : make_scorer(roc_auc_score), 
           'Sensitivity': make_scorer(sensitivity),
           'Specificity' : make_scorer(specificity)
          }

def compute_metrics_cv(scores):
    m2 = []
    scores.pop("fit_time")  #deleted beacause not necessary
    scores.pop("score_time")  #deleted beacause not necessary
    for i in sorted(scores.keys()):
        mean = round(scores[i].mean(), 2)
        std = round(scores[i].std()*2, 2)
        m2.append(str(mean) + " (+/- " + str(std) + ")" )
    return m2


# ## Classifiers

# Here you can specify the type of algoritm to balance dataset

# In[19]:


from imblearn.over_sampling import SMOTE
sm = SMOTE(random_state=1, k_neighbors=5, kind="borderline2")


# Classifiers

# In[20]:


from imblearn.pipeline import Pipeline as imbPipeline
from sklearn.model_selection import GridSearchCV, cross_validate    


# In[9]:


from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

def algorithms():
    pipe_lr = imbPipeline([
            ('oversample', sm),
            ('clf', LogisticRegression(solver="liblinear", n_jobs=-1))
            ])

    pipe_svm = imbPipeline([
            ('oversample', sm),
            ('clf', SVC(probability=True, random_state=1))
            ])

    param_grid_lr = {'clf__C': [0.01,0.1,1,10,100,1000,10000]}
    param_grid_svm = {'clf__C': [0.01,0.1,1,10,100,1000,10000]}

    algoritm_names = ["LOGISTIC REGRESSION", "SVM"]

    pipes = [pipe_lr, pipe_svm]
    param_grids = [param_grid_lr, param_grid_svm]

    for (name, pipe, param_grid) in zip(algoritm_names, pipes, param_grids):
        print("\n>>> %s" % name)
        gs = GridSearchCV(pipe, param_grid, scoring='roc_auc', cv=10, verbose=0, n_jobs=-1)
        gs.fit(X_train, y_train)
        #print("Best params: %s\n" % gs.best_params_)

        clf = gs.best_estimator_

        clf.fit(X_train, y_train)
        predictions = clf.predict(X_test)
        predictions_scores = clf.predict_proba(X_test)[:,1]

        # scores on entire dataset
        m1 = compute_metrics_es(y_test, predictions, predictions_scores)    

        # scores with stratified cross validation
        scores = cross_validate(clf, X, y, scoring=scoring, cv=10, return_train_score=False)
        m2 = compute_metrics_cv(scores)

        metrics = ["Accuracy", "Precision", "Auc", "Sensitivity", "Specificity"]
        d = {'METRIC': metrics, 'ENTIRE DATASET': m1, "STRATIFIED CV": m2}
        d = pd.DataFrame(d)
        d = d[['METRIC','ENTIRE DATASET', 'STRATIFIED CV']]
        print(d)


# ## Results SVM and LR (for each "Scuola")

# In[10]:


from sklearn.model_selection import train_test_split

for scuola in scuole:
    print("************   CODICE SCUOLA: %s   ************\n" % scuola)
    dataset = scuole[scuola]
    
    X = dataset.drop('Abbandoni', axis=1)
    y = dataset['Abbandoni']
    
    target_count = y.value_counts()
    print('Class 0:', target_count[0])
    print('Class 1:', target_count[1])
    print('Proportion:', round(target_count[0] / target_count[1], 2), ': 1')
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
    
    algorithms()
    
    print("\n")
    


# ## Results XGBoost (for each "Scuola")

# In[26]:


from xgboost import XGBClassifier

for scuola in scuole:
    print("************   CODICE SCUOLA: %s   ************\n" % scuola)
    dataset = scuole[scuola]
    
    X = dataset.drop('Abbandoni', axis=1)
    y = dataset['Abbandoni']
    
    target_count = y.value_counts()
    print('Class 0:', target_count[0])
    print('Class 1:', target_count[1])
    print('Proportion:', round(target_count[0] / target_count[1], 2), ': 1')
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
    
    clf = imbPipeline([
        ('oversample', sm),
        ('clf', XGBClassifier(n_jobs=-1))
    ])

    clf.fit(X_train.values, y_train)
    predictions = clf.predict(X_test.values)
    predictions_scores = clf.predict_proba(X_test.values)[:,1]

    # scores on entire dataset
    m1 = compute_metrics_es(y_test, predictions, predictions_scores)    

    # scores with stratified cross validation
    scores = cross_validate(clf, X.values, y, scoring=scoring, cv=10, return_train_score=False)
    m2 = compute_metrics_cv(scores)

    metrics = ["Accuracy", "Precision", "Auc", "Sensitivity", "Specificity"]
    d = {'METRIC': metrics, 'ENTIRE DATASET': m1, "STRATIFIED CV": m2}
    d = pd.DataFrame(d)
    d = d[['METRIC','ENTIRE DATASET', 'STRATIFIED CV']]
    print("\n")
    print(d)
    
    print("\n\n")

