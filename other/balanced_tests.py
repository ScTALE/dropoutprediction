#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Imbalanced Classification Problem
#Import the necessary libraries

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix, recall_score, roc_auc_score, precision_score, accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegression

from sklearn.ensemble import RandomForestClassifier

from imblearn.over_sampling import SMOTE

from decimal import *
getcontext().prec = 17


# ### TOOLS

# In[2]:


def compute_metrics(actual, predictions, predictions_scores):
    cm1 = confusion_matrix(y_true=actual, y_pred=predictions)
    print("CONFUSION MATRIX")
    fig, ax = plt.subplots(figsize=(2.5, 2.5))
    ax.matshow(cm1, cmap=plt.cm.Blues, alpha=0.3)
    for i in range(cm1.shape[0]):
        for j in range(cm1.shape[1]):
            ax.text(x=j, y=i, s=cm1[i, j], va='center', ha='center')

    plt.xlabel('predicted label')
    plt.ylabel('true label')

    plt.tight_layout()
    # plt.savefig('./figures/confusion_matrix.png', dpi=300)
    plt.show()

    print('SPECIFICITY: ', Decimal(cm1[1,1].item()) / (Decimal(cm1[1,0].item()) + Decimal(cm1[1,1].item())) )
    print('SENSITIVITY: ', Decimal(cm1[0,0].item()) / (Decimal(cm1[0,0].item()) + Decimal(cm1[0,1].item())) )
    print('PRECISION: ', precision_score(actual, predictions))
    print('ACCURACY: ', accuracy_score(actual, predictions))

    false_positive_rate, true_positive_rate, thresholds = roc_curve(actual, predictions_scores)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    plt.title('Receiver Operating Characteristic')
    plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f'% roc_auc)
    plt.legend(loc='lower right')
    plt.plot([0,1],[0,1],'r--')
    plt.xlim([-0.1,1.2])
    plt.ylim([-0.1,1.2])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()


# In[3]:


def compute_prediction_classes_count(predictions):
    x = np.bincount(predictions)
    ii = np.nonzero(x)[0]
    print("\nPREDICTIONS FOR EVERY CLASS:")
    print(np.vstack((ii,x[ii])).T)


# ### Lettura dataset

# In[4]:


dataset1 = pd.read_excel("/Users/antonio/GitHub/ia/data.xlsx")
dataset1.head()


# ### Preprocessing dati

# In[5]:


#Clean dataset
dataset = dataset1.drop(columns = ["ID_studente", "Voto_scuola_sup", "Situazione_OFA"] )
dataset.head()


# In[6]:


#Find out number of values in each class. 
dataset['Abbandoni'].value_counts()


# ### Split test-training set

# In[7]:


#The ratio of 0s to 1s is 1:7. This is a clear case of Imbalances class classification
X = dataset.drop('Abbandoni', axis=1)
y = dataset['Abbandoni']


# In[8]:


#Being imbalanced classification, we cannot ensure the predictions will be correct for the minority class
#So we will perform a SMOTE to balance out the two classes. Lets split the dataset into train and test before that
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)


# In[9]:


y_test.value_counts()


# ## Bilanciamento dataset

# In[10]:


#Smote will only be applied on the training dataset
print('BEFORE OVERSAMPLING:')
print('Count of labels with 0: {}'.format(sum(y_train==0)))
print('Count of labels with 1: {}\n\n'.format(sum(y_train==1)))


sm = SMOTE(random_state=2, k_neighbors=5, kind="svm")
#sm = SMOTE(random_state=2, k_neighbors=5, kind="borderline2")
X_train_res, y_train_res = sm.fit_sample(X_train, y_train.ravel())

print('AFTER OVERSAMPLING:')
print('Count of labels with 0: {}'.format(sum(y_train_res==0)))
print('Count of labels with 1: {}\n'.format(sum(y_train_res==1)))


# # TESTS

# ### Random Forests 

# In[11]:


clf = RandomForestClassifier(n_estimators=100, random_state=0)
clf.fit(X_train_res, y_train_res.ravel())

actual = y_test
predictions = clf.predict(X_test)
predictions_scores = clf.predict_proba(X_test)[:,1] # needed for roc calc

compute_metrics(actual=actual, predictions=predictions, predictions_scores=predictions_scores)
compute_prediction_classes_count(predictions=predictions)


# ### Regressione logistica con K-fold cross validation

# In[12]:


#Perform a Kfold cross validation and train/test the folds using LogisticRegression algorithm
parameters = {'C':(0.01, 0.1, 2, 10, 50)}
lr = LogisticRegression(solver="liblinear")
clf = GridSearchCV(lr, parameters, cv=5)
clf.fit(X_train, y_train) 


# In[13]:


clf.best_params_


# In[20]:


lr1 = LogisticRegression(C=0.01, penalty='l2', verbose=0, solver="liblinear")
lr1.fit(X_train_res, y_train_res.ravel())

actual = y_test
predictions = lr1.predict(X_test)
predictions_scores = lr1.predict_proba(X_test)[:,1] # needed for roc calc

compute_metrics(actual=actual, predictions=predictions, predictions_scores=predictions_scores)
compute_prediction_classes_count(predictions=predictions)


# ### XGBoost

# In[16]:


X_test.values


# In[19]:


from xgboost import XGBClassifier

model = XGBClassifier()
model.fit(X_train_res, y_train_res.ravel())

actual = y_test
predictions = model.predict(X_test.values)
predictions_scores = model.predict_proba(X_test2)[:,1] # needed for roc calc

compute_metrics(actual=actual, predictions=predictions, predictions_scores=predictions_scores)
compute_prediction_classes_count(predictions=predictions)


# In[ ]:




