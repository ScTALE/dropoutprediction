#!/usr/bin/env python
# coding: utf-8

# ### Import librerie e caricamento del dataset

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

dataset_path = "/Users/antonio/GitHub/ia/data.xlsx"
dataset = pd.read_excel(dataset_path)
dataset.head()


# ### Eliminate feature che non servono

# In[2]:


#dataset = dataset.drop(columns=['ID_studente', 'Voto_scuola_sup', 'Situazione_OFA'])
dataset = dataset.drop(columns=['ID_studente', 'Voto_scuola_sup'])


# ### Plot delle distribuzioni a coppia delle features

# In[3]:


sns.set(style='whitegrid', context='notebook')
col_names = dataset.columns.values
sns.pairplot(dataset[col_names], size=2.5)
plt.show()


# ### Calcolo e visualizzazione a griglia delle correlazioni a coppia tra features (Spearman)

# In[4]:


cm = dataset.corr(method="spearman")
sns.set(font_scale=1)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 8}, yticklabels=col_names,
                 xticklabels=col_names)
plt.show()


# ### Calcolo e visualizzazione a griglia delle correlazioni a coppia tra features (Pearson)

# In[5]:


cm = dataset.corr(method="pearson")
sns.set(font_scale=1)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 8}, yticklabels=col_names,
                 xticklabels=col_names)
plt.show()


# ## Cosa c'è nel dataset?

# In[6]:


dataset.isnull().sum()


# In[7]:


#sostituiti i dati mancanti con medianas
dataset.fillna(dataset.median(), inplace=True)
dataset.isnull().sum()

